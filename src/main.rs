use claxon::{FlacReader, FlacReaderOptions};
use std::env;
use std::fs;
use std::io::Result;
use std::path::{Path, PathBuf};
use std::process::Command;

fn main() -> Result<()> {
    let p = PathBuf::from(env::args().nth(1).expect("Folder Path")).canonicalize()?;
    let to = env::args()
        .nth(2)
        .map(|v| v.into())
        .unwrap_or(p.join("output/"));

    fs::create_dir_all(&to)?;

    fs::read_dir(p)?
        .filter_map(|v| v.ok()?.path().into())
        .filter(|p| is_flac(p))
        .filter(|p| get_bits_per_sample(p).unwrap_or(0) > 16)
        .inspect(|v| println!("Processing {:?}", v))
        .try_for_each(|p| downscale(&p, to.as_ref()))
}

fn is_flac(p: &Path) -> bool {
    p.extension()
        .filter(|e| e.to_ascii_lowercase() == "flac")
        .is_some()
}

fn get_bits_per_sample(p: &Path) -> claxon::Result<u32> {
    const OPT: FlacReaderOptions = FlacReaderOptions {
        metadata_only: true,
        read_vorbis_comment: false,
    };

    FlacReader::open_ext(p, OPT).map(|flac| flac.streaminfo().bits_per_sample)
}

fn downscale(p: &Path, to: &Path) -> Result<()> {
    let save_to = if to.is_dir() {
        to.join(p.file_name().unwrap())
    } else {
        to.to_owned()
    };

    Command::new("ffmpeg")
        .arg("-i")
        .arg(p.as_os_str())
        .arg("-af")
        .arg("aresample=out_sample_fmt=s16:out_sample_rate=48000")
        .arg(save_to.as_os_str())
        .output()?;

    Ok(())
}
