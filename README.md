# flac16
A simple program for me to save some spaces in my phone

## Use

[Rust](https://rust-lang.org/) and [FFmpeg](https://ffmpeg.org/) **must be installed**

<br>

Run this command

```sh
cargo run --release -- path/to/flac/folder 
```

After running the command, a folder named `output` will be created inside of 
that folder and it contains all the resampled flac and you can choose which one 
to replace the original one.

#### Note
Only the flac file with bits per sample value is more than 16 will be processed. <br>
Any file that isn't 24bit nor flac will not be processed

## Depedencies
- Claxon - Very cool flac decoder to get the bits per sample value
